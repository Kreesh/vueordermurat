import orderItem from '@/components/orderItem'
import deleteButton from '@/components/UI/deleteButton'
import orderMore from '@/components/UI/orderMore'
import shipmentMore from '@/components/UI/shipmentMore'
import MyDialog from '@/components/UI/MyDialog'

export default [ 
    orderItem,
    deleteButton,
    orderMore,
    shipmentMore,
    MyDialog
]